---
showTitle: true
title: download
bees: What if you and bees had highly different taste in music? 
---

* git <https://git.meli.delivery/meli/meli.git>
* read-only github mirror <https://github.com/meli/meli>

## install
<h4 id="debian">Debian</h4>
<p>Download an <code>amd64</code> <code>deb</code> package <a href="https://github.com/meli/meli/releases/">here</a>. Contributions for more packaging is welcome.</p>
<h4 id="binaries">static <code>amd64 linux</code> binary</h4>
<p><a href="https://github.com/meli/meli/releases/">https://github.com/meli/meli/releases/</a> </p>
<h4>other prebuilt binaries</h4>
<p>not available yet. Compile from <a href="https://git.meli.delivery/meli/meli.git">source</a> or install with <code>cargo</code>.</p>

### cargo{#cargo}
Note: `cargo` cannot distribute documentation. Install with `--features="cli-docs"` with `mandoc` or `man` in your system to use the command-line flags `--print-documentation [main config themes]` to print documentation in your terminal.

```sh
cargo install meli
```

## build
Build instructions can be found in the `README` file, but for convenience here's how to get it up and running:

```sh
git clone --depth=1 https://git.meli.delivery/meli/meli.git
cd meli
# print help
make
# try without installing:
# make meli
# ./target/release/meli
make PREFIX=$HOME/.local install
```

Read the [Quick start tutorial](documentation.html#quick-start). Documentation is located in the man pages and mirrored online [here](documentation.html).

Developed with focus on Linux for now, but if you get compilation errors or find bugs in your \*nix please report it in [meli-general](https://lists.meli.delivery) or [our issue tracker](https://git.meli.delivery/meli/meli/issues).


## contributing
There are no concrete contribution guidelines yet as the development workflow is still in its infancy. Mail your [patches](https://git-scm.com/docs/git-format-patch) to [meli-devel](https://lists.meli.delivery).

## bugs/support/feature requests
 If you encounter a bug or a lack of features, you are encouraged to contact [meli-general](https://lists.meli.delivery). You don't need to subscribe to the mailing lists, but your message will have to be approved first.

 Alternatively, you can post on our [issue tracker](https://git.meli.delivery/meli/meli/issues) <em>without registering</em> by sending an e-mail to `issues@` this address with the title of your report as the Subject and your text in the body. To hide your name and address while still being able to control your report, send your e-mail to `issues+anonymous@` instead.

 By sending an e-mail to `issues@`, you will receive a special password you can use to control your report with a link to your issue. You will be automatically subscribed to replies to this issue. You can use the password to:

 - close the issue `issues+${password}+close@`
 - subscribe/unsubscribe to replies `issues+${password}+unsubscribe@`, `issues+${password}+subscribe@`
 - reply `issues+${password}+reply@` (if your posting is anonymous, you will remain so)

 If you encounter issues with `issues@` you can fall back to [meli-general](https://lists.meli.delivery) and report this as well.
