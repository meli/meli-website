---
title: mailing lists
bees: What if bees abstained from voting?
---
## contact/discuss/support
[mailing lists:](https://lists.meli.delivery)

  * mailing list `<meli-general@at this domain>` [subscribe](https://lists.meli.delivery/mailman3/lists/meli-general.meli.delivery/)
  * development list `<meli-devel@at this domain>` [subscribe](https://lists.meli.delivery/mailman3/lists/meli-devel.meli.delivery/)
  * announcement list `<meli-announce@at this domain>` [subscribe](https://lists.meli.delivery/mailman3/lists/meli-announce.meli.delivery/)


[looking to contribute?](/download.html#contributing)

## archives
[https://lists.meli.delivery/archives/](https://lists.meli.delivery/archives/)

## IRC
there might also be people idling in `#meli` on freenode.
